// Imports
use rand::Rng;
use std::cmp::Ordering;
use std::io;

// Function
fn main() 
{
    // Printa as mensagens para o console
    println!("Adivinhe o numero!");
    println!("Digite um numero de 1 a 100:");

    // Gera um numero de 1 a 100 
    let secret_number = rand::thread_rng().gen_range(1, 101);  // .gen_range(min, max + 1);

    // Inicia um loop 
        // Desse modo o jogador pode continuar chutando
    loop 
    {
        // Cria a variavel de input
        let mut guess = String::new();
    
        // Try/Catch 
        io::stdin()
            .read_line(&mut guess)
            .expect("Erro ao ler a linha.");
    
        // Shadowing na variavel de input
            // Shadowing significa mudar o valor de uma variavel ja existente usando let
        // Match para String para detectar se o numero e valido
        let guess: u8 = match guess.trim().parse() 
        {
            Ok(num) => num,
             Err(_) => 
             {
                println!("Digite um numero valido.");
                continue;
             }
        };
    
        // Match com o input e o numero gerado para determinar se esta correto
        match guess.cmp(&secret_number) 
        {
            Ordering::Less => println!("Muito baixo!"),
            Ordering::Greater => println!("Muito alto!"),
            Ordering::Equal => 
            {
                // Caso o numero seja certo, break; para acabar o programa. 
                println!("Voce ganhou! O numero era {}.", secret_number);
                break;
            }
        }
    }
    
}
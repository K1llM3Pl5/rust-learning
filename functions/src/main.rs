// Funções
    
    // Funções são grupos de ações que podem ser criados fora do grupo
    // principal (main) para otimizar ações ou simplesmente continuar um
    // código.

    // Não importa aonde eu inicialize as funções, o que importa é a ordem
    // em que chamo elas na função principal.
    
    // Parâmetros

        // Parâmetros são os acompanhamentos que colocamos em funções.
        // Podemos vê-los nos parênteses, e servem para definirmos 
        // variáveis que estarão presentes em seu código.

        // É obrigatório definir o tipo de parâmetro que estamos usando
        // ao declará-los.

    // No final, devemos chamar as funções secundárias dentro da função 
    // principal para elas serem executadas (em ordem).

    // Retorno

        // Também podemos criar funções que retornam um valor quando chamadas.

        fn retorno() -> i8 // O -> indica a variável que iremos retornar.
        {
            5
        }

        // Nesse caso, a função irá retornar uma i8 com valor de 5 quando chamada. 
        // Possuímos dois modos de chamá-la. Mais em main. 

// Função principal
fn main() 
{
    println!("Main function start."); // Indicamos que a função principal foi ativada
    new_function_one(); // Chamando a primeira função
    new_function_two(5); // Chamando a segunda função e definindo o valor dos parâmetros
    new_function_three(10, 11); // Chamando a terceira função e definindo o valor dos parâmetros
    
    // Retorno

        let r = retorno(); // Chamando ao atribuir com uma variável
        println!("Retorno returned {} in r.", r);

        println!("Retorno returned {}", retorno()); // Ou podemos chamá-la simplesmente chamando normalmente

}

// Função secundária sem parâmetros
fn new_function_one() 
{
    println!("New function one start."); // Indicamos que a função secundária foi ativada
    println!("No parameters here, just me.");
}

// Função secundária com um parâmetro
fn new_function_two(i: i8) 
{
    println!("New function two start."); // Indicamos que a função secundária foi ativada
    println!("The value of i was defined as {}", i);
}

// Função secundária com múltiplos parâmetros
fn new_function_three(a: i8, b: u8)
{
    println!("New function three start."); // Indicamos que a função secundária foi ativada
    println!("The value of a was defined as {}", a);
    println!("The value of b was defined as {}", b);
}

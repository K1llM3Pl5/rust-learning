fn main() 
{
    println!("DEBUG: Starting program."); 

    let n1 = 3; // Inicializando nossa variável

    // Para controlar o fluxo do programa, podemos adicionar
    // condições para um código ser lido. Isso é feito usando
    // primariamente o "if".
    // Basicamente ele define uma condição para o código ser lido.
    // Caso a condição não se cumpra, ele segue o código.

    // Também podemos utilizar o else para acompanhar o if. Isso irá
    // fazer com que o código que coloquemos aqui seja lido se a condição
    // do if não for cumprida. Vale lembrar que isso é diferente de apenas
    // seguir o código, e uma das ações inseridas vai ser lida.

    if n1 > 5 // Neste caso, a condição é que n1 seja maior que 5.
    {
       println!("N1 was higher than 5.");
    }
    else // Caso n1 não seja maior que 5, logicamente ele será menor que 5.
    {
       println!("N1 was lower than 5.");
    }

    if n1 != 0 // Também temos que lembrar que nem sempre precisamos usar o else.
    {
       println!("N1 is not zero.");
    }

    // Também temos a possibilidade de usar o "else if". Isso vai fazer com
    // que o código seja lido se a condição anterior não for verdade *e* a condição
    // imposta depois do else também seja verdade. Deste jeito, caso nenhuma
    // das condições for verdadeira, nenhum código será lido.

    if n1 == 1 // Se n1 for igual a 1
    {
        println!("N1 is equal to 1.");
    }
    else if n1 == 2 // Se n1 for igual a 2
    {
        println!("N1 is equal to 2.");
    }
    else if n1 == 3 // Se n1 for igual a 3
    {
        println!("N1 is equal to 3.");
    }

    // Também podemos usar o if com let para definir o valor de uma variável:

    let condition = true; // Boolean

    // Caso não comparemos o boolean com nada,
    // o programa irá comparar o bool com true. 
    let n1 = if condition {8} else {10}; 
    println!("The new value of n1 is {}", n1);

    // Repetições

        // Existem diversos tipos de loops que podemos usar para repetir o programa.

        // Loop

            // O loop indica ao rust para continuar repetindo um código até
            // o programa parar ou se você mandar ele explícitamente parar.

            // Para rodar um loop, seria feito algo assim: 

                /*
                
                loop
                {
                    println!("Estou ficando tonto...");
                }
                
                */

            // Apenas não fiz isso em código pois isso iria ficar infinitamente mandando
            // a mensagem no terminal. A não ser que eu usasse o comando break.  
            // O comando break indica ao loop que é hora de parar. 
            // Um exemplo: 

                /*

                loop
                {
                    println!("De novo não...");
                    break;
                }

                println!("Ufa, finalmente!");
            
                // Aqui o código continuaria normalmente.

                */

            // Assim, podemos combinar tudo isso com o if, que aprendemos mais cedo.
                
                let mut loops = 0;
                println!(); // Meramente para dar espaço
                loop
                {
                    loops += 1; // Fazer com que a variavel suba 1 no valor a cada loop
                    println!("Looping!");
                    
                    if loops == 5 
                    {
                        break;
                    }
                }
                println!("Hey, we stopped!");
                println!(); // Meramente para dar espaço
        
        // While
                
            // Assim como o loop, o while nos permite repetir códigos, 
            // mas dessa vez, ele possui alguns atributos do if, e apenas
            // irá loopar enquanto uma certa condição for verdadeira ou falsa.

            // Exemplo:

                let mut countdown = 10;
                println!(); // Meramente para dar espaço
                println!("Iniciating countdown."); 

                while countdown != 0 // Enquanto countdown for diferente de 0, ele vai loopar
                {
                    println!("{}.", countdown);
                    countdown -= 1; // Reduz o valor de countdown por 1 a cada loop
                }

                println!("LIFTOFF!"); // O while acabou, e podemos continuar nosso código.

        // For
                
            // Ok, concluímos o while, porém, por incrível que pareça, o código anterior
            // está mal otimizado. Não há porquê usar o while para isso. Ao invés disso, 
            // podemos usar o for.

            // O for funciona como o while, porém a sua condição é que ele irá loopar um 
            // número de vezes determinadas, como, por exemplo, uma contagem de 10 a 1.

            // Exemplo: 

                println!(); // Meramente para dar espaço
                println!("Iniciating countdown, this time using for."); 

                for number in (1..11).rev() // Aqui temos que desconstruir algumas coisinhas. Mais abaixo.
                {
                    println!("{}.", number);
                }

                println!("LIFTOFF!"); // O for acabou, e podemos continuar nosso código.
            
                // Ok então, vamos olhar mais de perto para aquele for. Começamos criando
                // a variável number, que irá ter o valor que o for definir. No nosso caso,
                // demos para o for um alcance. Ele irá contar dentre os números que colocamos,
                // no caso 1 e 10 (11 pois é o segundo número + 1). Ele contaria de 1 a 10, porém
                // revertemos a contagem usando .rev(), então ele contará regressivamente.
    
    // Sinais

        // Para concluir, acho que uma rápida explicação e demonstração de alguns tipos de sinais e
        // como usá-los seria útil. 
        
            // ! - diferente
            // = - igual
            // < - menor
            // > - maior
            
            // Dito isso, existe uma diferença no uso dos sinais. Caso você não tenha percebido, 
            // num if, por exemplo, fazemos a < b, porém no while ou loops, fazemos a == b ou
            // a != b. Isso acontece pois, para compararmos diretamente dois valores (igual ou diferente)
            // precisamos usar o sinal duas vezes. Além disso, podemos combinar sinais. Alguns exemplos:

                // a != b   ---   a diferente de b
                // a == b   ---   a igual a b
                // a <= b   ---   a menor ou igual a b
                // a >= b   ---   a maior ou igual a b
                
            // Também usamos o = em contas, especialmente em loops, por exemplo: 

                // a -= 1;  ---  a = a - 1 cada vez que o loop for feito.
                // a += b;  ---  a = a + b cada vez que o loop for feito.      
}
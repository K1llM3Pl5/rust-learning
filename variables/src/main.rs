fn main() 
{
    // Constantes

        // Constantes são variáveis imutáveis que terão sempre um valor
        // fixo e verdadeiro enquanto o programa rodar. 

        const CONSTANT: u32 = 100;
        println!("The value of CONSTANT is {}", CONSTANT);

    // Shadowing

        // Shadowing é o ato de mudar o valor de uma variável já
        // existente utilizando "let". Desse jeito, podemos fazer 
        // transformações numa variável.

        // Assim, podemos até fazer transformações em valores e tipos
        // de variáveis imutáveis. 
        let x = 5;
        let x = x + 1;
        let x = x * 3;
        println!("The value of x is {}", x);

        let spaces = "   ";
        let spaces = spaces.len();
        println!("The number of spaces is {}", spaces);

    // Data Types

        // O Rust precisa saber o tipo de todas as variáveis
        // que vamos usar, por isso existem diversos tipos de data.

        // Integers

            // Integers são numeros não-fracionarios. Podemos fazer
            // ints com diversos tipos de bits, assinados ou não
            // assinados.

            // Bits 
                
                // Podem variar entre 8, 16, 32, 64, 128 ou arch.
                // Arch equivale a isize ou usize (mais em assinaturas)
                // e define o tamanho de bits conforme a arquitetura
                // do seu sistema.

            // Assinaturas

                // Integers possuem um tamanho explicitado. A assinatura
                // nos permite dizer se o número pode ou não ser negativo.

                // Números assinados (signed) precisam conter um sinal
                // ao lado, para explicitar se são positivos ou negativos.  
                // Já, numeros não-assinados (unsigned) devem ser sempre
                // positivos, já que não precisam ser acompanhados por
                // um sinal.
        
            // Exemplos

                // i32 = Assinado com 32 bits.
                // u8 = Não-assinado com 8 bits.

        // Floatings
            
            // Floatings são numerais com casas decimais. 
            
            // Tipos

                // O Rust possui dois tipos de floatings:
                // f32 e f64 (32 e 64 bits). O tipo padrão é f64 pois
                // em CPUs modernas tem quase a mesma velocidade do f32
                // porem é mais preciso.
            
                // Exemplos

                let float64 = 2.0; // f64
                let float32: f32 = 2.0; // f32

                println!("The value of float64 is {}", float64);
                println!("The value of float32 is {}", float32);
        
        // Operações numericas exemplificadas

            // Juntando os floatings e integers até agora, podemos fazer 
            // alguns cálculos.

            // Adição
            let sum = 3 + 7;

            // Subtração
            let difference = 10.9 - 6.0;

            // Produto
            let product = 5 * 10;

            // Divisão
            let quotient = 10 / 2;

            // Printing
            println!("The value of the sum is {}", sum);
            println!("The value of the subtraction is {}", difference);
            println!("The value of the multiplication is {}", product);
            println!("The value of the division is {}", quotient);
        
        // Booleans

            // Booleans são variáveis que variam entre verdadeiras e falsas,
            // (true ou false) e tem o tamanho de um byte. Elas sao representadas
            // utilizando "bool".
            
            // São usados primariamente em expressões de "if".

            // Exemplos

                let v = true; // Não explícito
                let f: bool = false; // Explícito

                println!("The value of v is {}", v);
                println!("The value of f is {}", f);

        // Caracteres (Character)

            // Os caracteres são variáveis que podem conter apenas uma letra
            // ou caractere. Isso pode variar desde ASCII até caracteres japoneses
            // e emojis.

            // Exemplo

                let char1 = 'z'; // Não-explícito e ASCII
                let char2: char = 'ℤ'; // Explícito e não-ASCII
                let emoji = '😻'; // Não-explícito e não-ASCII (emoji)

                println!("The char in char1 is {}", char1);
                println!("The char in char2 is {}", char2);
                println!("The char in emoji is {}", emoji);

        // Tuples

            // As Tuples são formas de agrupar valores de diversos tipos
            // em um tipo composto. 
            // Elas possuem um tamanho fixo, e não se pode aumentar ou diminuir
            // a sua quantia de valores.
            // Tambem, não se pode adquirir o valor da Tuple pois é um valor
            // composto. Por isso, devemos adquirir os valores individuais.

            //Exemplo:

                let tuple: (i16, f64, u8) = (500, 6.4, 1); // Declarando a tuple
                
                let (xtup, ytup, ztup) = tuple; // Atribuímos os valores da tuple a variáveis 

                println!("The value of xtup is {}", xtup);
                println!("The value of ytup is {}", ytup);
                println!("The value of ztup is {}", ztup);

            // Também podemos acessar os valores individuais usando um ponto (.)
            // na tuple a atribuí-los a outras variáveis caso queiramos.

            //Exemplo:

                let tuple2: (i32, f64, u8) = (600, 7.4, 2);

                let value1 = tuple2.0;
                
                println!("The value of value1 is {}", value1); // Atribuindo a uma variável
                println!("The third value of tuple2 is {}", tuple2.2); // Não atribuindo a uma variável

        // Arrays 

            // Arrays são um outro modo de agrupar valores. Porém, diferente
            // das tuples, eles devem conter valores do mesmo tipo.
            // Além disso, no Rust eles são diferentes de outras línguas pois,
            // assim como as tuples, eles tem um tamanho fixo. 

            // Exemplo:

                // Instanciando
                let arraynumbers = [1, 2, 3];  
                let arraydays = ["Sunday", "Monday", "Tuesday",
                                 "Wednesday", "Thursday", "Friday", "Saturday"];

                // Acessando
                let firstnumber =  arraynumbers[0];
                let secondday = arraydays[1];

                // Printando
                println!("The first number in arraynumbers is {}", firstnumber);
                println!("The second day in arraydays is {}", secondday);
}
